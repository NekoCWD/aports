# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=font-iosevka
pkgver=30.0.0
pkgrel=0
pkgdesc="Versatile typeface for code, from code"
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-aile
	$pkgname-etoile
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-Iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-IosevkaAile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-IosevkaEtoile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-IosevkaSlab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-SGr-IosevkaCurly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/PkgTTC-SGr-IosevkaCurlySlab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base=$pkgver-r$pkgrel
		$pkgname-aile=$pkgver-r$pkgrel
		$pkgname-etoile=$pkgver-r$pkgrel
		$pkgname-slab=$pkgver-r$pkgrel
		$pkgname-curly=$pkgver-r$pkgrel
		$pkgname-curly-slab=$pkgver-r$pkgrel
		"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/Iosevka-*.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/iosevka/IosevkaAile-*.ttc
}

etoile() {
	pkgdesc="$pkgdesc (Iosevka Etoile)"
	amove usr/share/fonts/iosevka/IosevkaEtoile-*.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/IosevkaSlab-*.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/SGr-IosevkaCurly-*.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/SGr-IosevkaCurlySlab-*.ttc
}

sha512sums="
48a62437651f8891f8fd720d5b816991df26c420b0fef783ce72409f5151c4cbc414d0b0da31bf706915e951d0b3eec764c6a8c15025c3eb93ff47ff56b577a4  PkgTTC-Iosevka-30.0.0.zip
4403629f7cf07fb04ad53cb923b95b9a29c1bf5ca0d85addcb8a8164531d3e14d807c54ad475f49c5e5e51ead7555a654b86ae494b2baebf24067f9aaecfcd4b  PkgTTC-IosevkaAile-30.0.0.zip
a4d8f82c7e88b1c1b4a4ee4536e0cce898cfcba9852fbe65589cd61788fed8a918de534bb8801ca5af0fdb08766ddd7f5ea0d7a0331837837f090aa35cf1c830  PkgTTC-IosevkaEtoile-30.0.0.zip
0aa4fe91abbdea1a532b98cf698a147f99b364245452e32fb436b84315d5c9e6063cac942d15a9e09ece2b777c8f4a6662feddc98cc7b32a7534381f4a5f426d  PkgTTC-IosevkaSlab-30.0.0.zip
2f23f3f666911b6eace73d079f7cba930a454fd9f8b009f9bc99f3c283d8c812fb69986c12bddc3eb1ffd2a756a62fb6c4a28046355ca1a59c8b9061608ab8e9  PkgTTC-SGr-IosevkaCurly-30.0.0.zip
d3e45d133304204262de8377d28698728810ab64d36d65bd598cc667e7cce59d516f76a58fa305583149b6eb20d5b907a61b255111dde99c2d182d11d5618091  PkgTTC-SGr-IosevkaCurlySlab-30.0.0.zip
"
