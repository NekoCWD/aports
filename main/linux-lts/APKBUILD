# Maintainer: Natanael Copa <ncopa@alpinelinux.org>

_flavor=lts
pkgname=linux-$_flavor
pkgver=6.6.30
case $pkgver in
	*.*.*)	_kernver=${pkgver%.*};;
	*.*) _kernver=$pkgver;;
esac
pkgrel=0
pkgdesc="Linux lts kernel"
url="https://www.kernel.org"
depends="initramfs-generator"
_depends_dev="perl gmp-dev mpc1-dev mpfr-dev elfutils-dev bash flex bison zstd"
makedepends="$_depends_dev sed installkernel bc linux-headers linux-firmware-any openssl-dev>3 mawk
	diffutils findutils zstd pahole python3 gcc>=13.1.1_git20230624"
options="!strip !check"
_config=${config:-config-lts.${CARCH}}
source="https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/linux-$_kernver.tar.xz
	0001-powerpc-boot-wrapper-Add-z-notext-flag-for-ppc64le.patch
	0002-x86-Compress-vmlinux-with-zstd-19-instead-of-22.patch
	0003-kexec-add-kexec_load_disabled-boot-option.patch
	0004-objtool-respect-AWK-setting.patch
	0005-powerpc-config-defang-gcc-check-for-stack-protector-.patch

	lts.aarch64.config
	lts.armv7.config
	lts.x86.config
	lts.x86_64.config
	lts.ppc64le.config
	lts.s390x.config
	lts.loongarch64.config

	virt.aarch64.config
	virt.armv7.config
	virt.ppc64le.config
	virt.x86.config
	virt.x86_64.config
	"
subpackages="$pkgname-dev:_dev:$CBUILD_ARCH $pkgname-doc"
for _i in $source; do
	case $_i in
	*.$CARCH.config)
		_f=${_i%."$CARCH".config}
		_flavors="$_flavors $_f"
		if [ "linux-$_f" != "$pkgname" ]; then
			subpackages="$subpackages linux-$_f::$CBUILD_ARCH linux-$_f-dev:_dev:$CBUILD_ARCH"
		fi
		;;
	esac
done
builddir="$srcdir"/linux-$_kernver

if [ "${pkgver%.0}" = "$pkgver" ]; then
	source="$source
	https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/patch-$pkgver.xz"
fi
arch="all !armhf !riscv64"
license="GPL-2.0-only"

# secfixes:
#   5.10.4-r0:
#     - CVE-2020-29568
#     - CVE-2020-29569
#   5.15.74-r0:
#     - CVE-2022-41674
#     - CVE-2022-42719
#     - CVE-2022-42720
#     - CVE-2022-42721
#     - CVE-2022-42722
#   6.1.27-r3:
#     - CVE-2023-32233
#   6.6.13-r1:
#     - CVE-46838

prepare() {
	if [ "$_kernver" != "$pkgver" ]; then
		msg "Applying patch-$pkgver.xz"
		unxz -c < "$srcdir"/patch-$pkgver.xz | patch -p1 -N
	fi

	default_prepare

	# remove localversion from patch if any
	rm -f localversion*
}

_kernelarch() {
	local arch="$1"
	case "$arch" in
		aarch64*) arch="arm64" ;;
		arm*) arch="arm" ;;
		mips*) arch="mips" ;;
		ppc*) arch="powerpc" ;;
		s390*) arch="s390" ;;
		riscv*) arch="riscv" ;;
		loongarch64) arch="loongarch" ;;
	esac
	echo "$arch"
}

_prepareconfig() {
	local _flavor="$1"
	local _arch="$2"
	local _config=$_flavor.$_arch.config
	local _builddir="$srcdir"/build-$_flavor.$_arch
	mkdir -p "$_builddir"
	echo "-$pkgrel-$_flavor" > "$_builddir"/localversion-alpine

	cp "$srcdir"/$_config "$_builddir"/.config
	msg "Configuring $_flavor kernel ($_arch)"
	make -C "$builddir" \
		O="$_builddir" \
		ARCH="$(_kernelarch $_arch)" \
		olddefconfig

	if grep "CONFIG_MODULE_SIG=y" "$_builddir"/.config >/dev/null; then
		if [ -f "$KERNEL_SIGNING_KEY" ]; then
			sed -i -e "s:^CONFIG_MODULE_SIG_KEY=.*:CONFIG_MODULE_SIG_KEY=\"$KERNEL_SIGNING_KEY\":" \
				"$_builddir"/.config
			msg "Using $KERNEL_SIGNING_KEY to sign $_flavor kernel ($_arch) modules"
		else
			warning "KERNEL_SIGNING_KEY was not set. A signing key will be generated, but 3rd"
			warning "party modules can not be signed"
		fi
	fi
}

listconfigs() {
	for i in $source; do
		case "$i" in
			*.config) echo $i;;
		esac
	done
}

prepareconfigs() {
	for _config in $(listconfigs); do
		local _flavor=${_config%%.*}
		local _arch=${_config%.config}
		_arch=${_arch#*.}
		local _builddir="$srcdir"/build-$_flavor.$_arch
		_prepareconfig "$_flavor" "$_arch"
	done
}

# this is supposed to be run before version is bumped so we can compare
# what new kernel config knobs are introduced
prepareupdate() {
	clean && fetch && unpack && prepare && deps
	prepareconfigs
	rm -r "$builddir"
}

updateconfigs() {
	if ! [ -d "$builddir" ]; then
		deps && fetch && unpack && prepare
	fi
	for _config in ${CONFIGS:-$(listconfigs)}; do
		msg "updating $_config"
		local _flavor=${_config%%.*}
		local _arch=${_config%.config}
		_arch=${_arch#*.}
		local _builddir="$srcdir"/build-$_flavor.$_arch
		mkdir -p "$_builddir"
		echo "-$pkgrel-$_flavor" > "$_builddir"/localversion-alpine
		local actions="listnewconfig oldconfig"
		if ! [ -f "$_builddir"/.config ]; then
			cp "$srcdir"/$_config "$_builddir"/.config
			actions="olddefconfig"
		fi
		env | grep ^CONFIG_ >> "$_builddir"/.config || true
		make -j1 -C "$builddir" \
			O="$_builddir" \
			ARCH="$(_kernelarch $_arch)" \
			$actions savedefconfig

		cp "$_builddir"/defconfig "$startdir"/$_config
	done
}

build() {
	unset LDFLAGS
	# for some reason these sometimes leak into the kernel build,
	# -Werror=format-security breaks some stuff
	unset CFLAGS CPPFLAGS CXXFLAGS
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"
	for i in $_flavors; do
		_prepareconfig "$i" "$CARCH"
	done
	for i in $_flavors; do
		msg "Building $i kernel"
		cd "$srcdir"/build-$i.$CARCH

		# set org in cert for modules signing
		# https://www.kernel.org/doc/html/v6.1/admin-guide/module-signing.html#generating-signing-keys
		mkdir -p certs
		sed -e 's/#O = Unspecified company/O = alpinelinux.org/' \
			"$builddir"/certs/default_x509.genkey \
			> certs/x509.genkey

		make ARCH="$(_kernelarch $CARCH)" \
			CC="${CC:-gcc}" \
			AWK="${AWK:-mawk}" \
			KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-Alpine"
	done
}

_package() {
	local _buildflavor="$1" _outdir="$2"
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

	cd "$srcdir"/build-$_buildflavor.$CARCH
	local _abi_release="$(make -s kernelrelease)"
	# modules_install seems to regenerate a defect Modules.symvers on s390x. Work
	# around it by backing it up and restore it after modules_install
	cp Module.symvers Module.symvers.backup

	mkdir -p "$_outdir"/boot "$_outdir"/lib/modules

	local _install
	case "$CARCH" in
		arm*|aarch64|riscv*) _install="zinstall dtbs_install";;
		*) _install=install;;
	esac

	make modules_install $_install \
		ARCH="$(_kernelarch $CARCH)" \
		INSTALL_MOD_PATH="$_outdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_PATH="$_outdir"/boot \
		INSTALL_DTBS_PATH="$_outdir/boot/dtbs-$_buildflavor"

	cp Module.symvers.backup Module.symvers

	rm -f "$_outdir"/lib/modules/"$_abi_release"/build \
		"$_outdir"/lib/modules/"$_abi_release"/source
	rm -rf "$_outdir"/lib/firmware

	install -D -m644 include/config/kernel.release \
		"$_outdir"/usr/share/kernel/$_buildflavor/kernel.release

	case "$CARCH" in
	loongarch64)
		mv  "$_outdir"/boot/vmlinuz-$_abi_release \
			"$_outdir"/boot/vmlinuz-$_buildflavor

		mv  "$_outdir"/boot/config-$_abi_release \
			"$_outdir"/boot/config-$_buildflavor

		mv  "$_outdir"/boot/System.map-$_abi_release \
			"$_outdir"/boot/System.map-$_buildflavor
		;;
	esac
}

# main flavor installs in $pkgdir
package() {
	depends="$depends linux-firmware-any"

	_package "$_flavor" "$pkgdir"

	# copy files for linux-lts-doc sub package
	mkdir -p "$pkgdir"/usr/share/doc
	cp -r "$builddir"/Documentation \
		"$pkgdir"/usr/share/doc/linux-doc-"$pkgver"/
	# remove files that aren't part of the documentation itself
	for nondoc in \
		.gitignore conf.py docutils.conf \
		dontdiff Kconfig Makefile
	do
		rm "$pkgdir"/usr/share/doc/linux-doc-"$pkgver"/"$nondoc"
	done
	# create /usr/share/doc/linux-doc symlink
	cd "$pkgdir"/usr/share/doc; ln -s linux-doc-"$pkgver" linux-doc
}

# subflavors install in $subpkgdir
virt() {
	_package virt "$subpkgdir"
}

_dev() {
	local _flavor=$(echo $subpkgname | sed -E 's/(^linux-|-dev$)//g')
	local _builddir="$srcdir"/build-$_flavor.$CARCH
	local _abi_release="$(make -C "$_builddir" -s kernelrelease)"
	# copy the only the parts that we really need for build 3rd party
	# kernel modules and install those as /usr/src/linux-headers,
	# simlar to what ubuntu does
	#
	# this way you dont need to install the 300-400 kernel sources to
	# build a tiny kernel module
	#
	pkgdesc="Headers and script for third party modules for $_flavor kernel"
	depends="$_depends_dev"
	local dir="$subpkgdir"/usr/src/linux-headers-"$_abi_release"
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

	# first we import config, run prepare to set up for building
	# external modules, and create the scripts
	mkdir -p "$dir"
	cp -a "$_builddir"/.config "$_builddir"/localversion-alpine \
		"$dir"/

	install -D -t "$dir"/certs "$_builddir"/certs/signing_key.x509 || :

	make -C "$builddir" \
		O="$dir" \
		ARCH="$(_kernelarch $CARCH)" \
		AWK="${AWK:-mawk}" \
		prepare modules_prepare scripts

	# remove the stuff that points to real sources. we want 3rd party
	# modules to believe this is the sources
	rm "$dir"/Makefile "$dir"/source

	# copy the needed stuff from real sources
	#
	# this is taken from ubuntu kernel build script
	# http://kernel.ubuntu.com/git/ubuntu/ubuntu-zesty.git/tree/debian/rules.d/3-binary-indep.mk
	cd "$builddir"
	find .  -path './include/*' -prune \
		-o -path './scripts/*' -prune -o -type f \
		\( -name 'Makefile*' -o -name 'Kconfig*' -o -name 'Kbuild*' -o \
		   -name '*.sh' -o -name '*.pl' -o -name '*.lds' -o -name 'Platform' \) \
		-print | cpio -pdm "$dir"

	cp -a scripts include "$dir"

	find "arch/$_karch" -name include -type d -print | while IFS='' read -r folder; do
		find "$folder" -type f
	done | sort -u | cpio -pdm "$dir"

	install -Dm644 "$srcdir"/build-$_flavor.$CARCH/Module.symvers \
		"$dir"/Module.symvers

	# remove unneeded things
	msg "Removing documentation..."
	rm -r "$dir"/Documentation
	find "$dir" -type f -name '*.o' -printf 'Removing %P\n' -delete
	local _karch="$(_kernelarch $CARCH | sed 's/x86_64/x86/')"
	msg "Removing unneeded arch headers..."
	for i in "$dir"/arch/*; do
		if [ "${i##*/}" != "$_karch" ]; then
			echo "  ${i##*/}"
			rm -r "$i"
		fi
	done

	mkdir -p "$subpkgdir"/lib/modules/"$_abi_release"
	ln -sf /usr/src/linux-headers-"$_abi_release" \
		"$subpkgdir"/lib/modules/"$_abi_release"/build
}

sha512sums="
458b2c34d46206f9b4ccbac54cc57aeca1eaecaf831bc441e59701bac6eadffc17f6ce24af6eadd0454964e843186539ac0d63295ad2cc32d112b60360c39a35  linux-6.6.tar.xz
58bf9e84fb01f811f0cada7f7953d111908f0b0650648d6a3522061fe08c9fe284c9315515ae386189253e37d0c92419f78048d6568e2e426654b1e61010685f  0001-powerpc-boot-wrapper-Add-z-notext-flag-for-ppc64le.patch
763dcb40f698d43b0bb856f44493973e840b66efe62a8fd41a08b6e3fa38cf903e059d107be3735ab2f38a0f27bdb0262798e5e530a1905f96195cd8bcf62fdf  0002-x86-Compress-vmlinux-with-zstd-19-instead-of-22.patch
75f232b6becee7d36d360ffaf2aaa837d13518f9ec620ca159bcb2a0e98eb18a77631406a9b44244ea0164a7ed59fad583823909681e9b894a012f9d13365b69  0003-kexec-add-kexec_load_disabled-boot-option.patch
2956050bb332411d00a285e9656618f2e34b631492bbc19bef54d83a6e91b8531f4e18830b9313cfe52fbf8a8ca6fb76cf55b3ddd146ca3b977046cf2fd10cad  0004-objtool-respect-AWK-setting.patch
4b16f15b47b5e5835b926126e39723f519290469517875cfb2d2b857a93ad1344f0d1ba4b255289e20f4da9c867647526b344f752981cee0a48c51577badac3f  0005-powerpc-config-defang-gcc-check-for-stack-protector-.patch
ae8f01abee26ab4b184f7686fccec6a2559facd1c3fabc1be47c5a4310ca48747b81a055973f98ac2e0129ca48169ae078b6b777490b6257fb9cf87cded96923  lts.aarch64.config
57e57fcc9463ce5360fd09af9b56d0679275edc2c846e470557abffb751314408c6bdad4f47b3a60d81c931e0e226bad685b59e217e73b80bd45077565dd90a2  lts.armv7.config
c0e402e6da43cb30ec153fc3f421e172533d7b99fb5c6598209dfbac154156020ce0e73db73edcd0356b352e63877c4170c5a7620bf913e5f8ecc600d235c4dc  lts.x86.config
c017c167a3f7e6169e3556eaa8e88f156a91de17e9c127fdbac3b54f8b4b1e025655f5ad8581a03994b0d2b27135c44ca228a3763858c0c2cf1b019da18dfbaf  lts.x86_64.config
5cd3a8265329da4a06aad545a48063e473e48691e2e96d3b836f4e83013d0621e373c47f3af3cf72b35b40d76bdd1ea0fda7d0b46750d79951071e26dd1d4361  lts.ppc64le.config
b8629d77112d22bce5237152549326ea1c08d3534824b75a39111e539bc28510ccfafa0839d74f7e7455d2530b69f963c84fc4d39ddc2278ce99c932798a8052  lts.s390x.config
36f927268a01f431634ffc4c732653e8ab2dbb3c877137fda714b65eacfbfd897a1f0aaa6c35c745217c94a29bfadf71fbfa95722dcf551e62742bff2e29a42b  lts.loongarch64.config
b7bd7caa856bc960f180682004685cc898bb176398ab2b832a9995225060161c01c377f596787d0113962611cd21c49689636180252d0b24247e7cf5b231f848  virt.aarch64.config
7e4cd192b30aed8e9d1f01c99a1350f09e7498ad31e332c0ff2d0b0291d70a787b80aee2105e6d3208f05066faee02a12cbd2fb82fc8e43dc9eec2baa84d35ff  virt.armv7.config
2c660dd87ad7cf7e730b3748e9cb0e33c50cdaf7a37779b2b2787160c633686761ee02f28a4c35ee4a4db8cac8d0516af7e06a2a85ebb891724d8f401fff592a  virt.ppc64le.config
ba056237fe5b1d18b0dbc242f609896e437d37dd9a6131f5257e2d9a706cac497f01fecf11e81b7a4319aa60cbf388f8a5c036c24cd65a6075aa1dd1f3aa1ffe  virt.x86.config
91b7c53c346c806affc30502ecf7088804afa706f8c123a469718bf7c74b250e10696b46413e173187c2a40cdcdd7a3d435f4af35ee45e7257794e70289861cd  virt.x86_64.config
f96b8db3029f0ae7207c9f4b60f96eb8d5f808b607bd3ecea940f63ea7de9d0be6e60524ddcedf5996ef89c7e202781b941fb88c76784d63a27a91eed3070cee  patch-6.6.30.xz
"
